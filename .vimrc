colorscheme elflord 
syntax on

set nocompatible
set number
set rnu
set ruler
set autoindent
set tabstop=4
set shiftwidth=4
set expandtab
set scrolloff=8
set sidescrolloff=16
set sidescroll=1
set wildmenu
set is hls
set listchars=tab:>·,trail:·,extends:>,precedes:<,space:·
set mouse=a
